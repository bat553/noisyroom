<?php
require_once  'include/control.php';
require_once  'include/database.php';
$stop = filter_input(INPUT_POST, 'stop', FILTER_VALIDATE_INT);
$room_name = filter_input(INPUT_POST, 'room_name', FILTER_SANITIZE_STRING);
$session_name = filter_input(INPUT_POST, 'session_name', FILTER_SANITIZE_STRING);

try {
    $running_session = getRunningSessions();
    if ($session_name and count($running_session) == 0){
        // Créer une nouvelle session
        print_r($running_session);
        newSession($session_name, $room_name);
        header('Location: /index.php');
        die();
    } else if ($stop and count($running_session) == 1){
        // Arrete la session en cours
        stopScreen($stop);
        header('Location: /index.php');
        die();
    } else {
        print('Merci de spécifier un paramètre');
    }
} catch (Exception $e){
    print "Erreur !: ".$e->getMessage()."<br/>";
    die();
}
