<?php

$username = 'noisyroom';
$password = 'noisyroom';

$ddb = new PDO('mysql:host=localhost;port=3306;dbname=noisyroom', $username, $password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);


function getRunningSessions(){
    global $ddb;
    try {
        $req = $ddb->query("SELECT * FROM sessions WHERE running = 1");
        $running_session = $req->fetchAll(PDO::FETCH_ASSOC);
        return $running_session;
    } catch (PDOException $e){
        print "Erreur !: ".$e->getMessage()."<br/>";
        die();
    }
}

function getSession($id_session){
    global $ddb;
    try {
        $req = $ddb->prepare("SELECT * FROM sessions WHERE id_session = :id_session");
        $req->bindParam(':id_session', $id_session, PDO::PARAM_INT);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC)[0];
    } catch (PDOException $e){
        print "Erreur !: ".$e->getMessage()."<br/>";
        die();
    }
}

function stopRunningSession($id_session){
    global $ddb;
    try {
        $req = $ddb->prepare("UPDATE `noisyroom`.`sessions` SET `running`='0' WHERE  `id_session`=:id_session");
        $req->bindParam(':id_session', $id_session, PDO::PARAM_INT);
        $req->execute();
    } catch (PDOException $e){
        print "Erreur !: ".$e->getMessage()."<br/>";
        die();
    }
}
