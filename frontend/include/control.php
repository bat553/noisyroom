<?php

require_once  'database.php';

function newSession($session_name, $room_name){

    $screen_name = explode(' ', $session_name)[0];
    exec("sudo screen -d -m -S '".$screen_name."' /usr/bin/python3 /var/noisyroom/backend/noisyroom.py '".$session_name."' '".$room_name."' > /dev/null &");
}

function stopScreen($id_session){
    $session_infos = getSession($id_session);

    stopRunningSession($session_infos['id_session']);

    $screen_name = explode(" ", $session_infos['name'])[0];

    exec("sudo screen -X -S ".$screen_name." quit");

}

