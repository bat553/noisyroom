<?php
require_once  'include/database.php';

sleep(2); // Attente du process python....
$running_session = getRunningSessions()


?>

<!DOCTYPE HTML>
<html lang="fr">
<head>
    <title>NoisyRoom - Admin Panel</title>
    <link href="static/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a href="#" class="navbar-brand">NoisyRoom - Admin</a>
    <div class="navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="/grafana" class="nav-link">Grafana</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <?php if (count($running_session)==0):?>
        <h1>Nouvelle session d'enregistrement</h1>
    <form action="action.php" method="post">
        <div class="form-group">
            <label for="session_name">Nom de la session <span class="text-danger">*</span></label>
            <input type="text" id="session_name" name="session_name" class="form-control" placeholder="Session 202">
        </div>
        <div class="form-group">
            <label for="room_name">Nom de la salle</label>
            <input type="text" id="room_name" name="room_name" class="form-control" placeholder="D216">
        </div>
        <button class="btn btn-info" type="submit">Démarrer</button>
    </form>

    <?php endif;?>
    <?php if (count($running_session)>1):?>
        <h1>Erreur ! Plusieurs session en cours :-/</h1>
    <?php endif;?>
    <?php if (count($running_session)==1):?>
    <form action="action.php" method="post">
        <h1>Session en cours <?=$running_session[0]['name']?></h1>
        <input name="stop" value="<?=$running_session[0]['id_session']?>" class="d-none">
        <button class="btn btn-danger">Arreter</button>
    </form>
    <?php endif;?>

</div>

</body>
</html>
