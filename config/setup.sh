#!/bin/bash
cat <<FIN
INSTALLATION SUR RASPBERRY
FIN
read

apt-get install -y adduser libfontconfig1
wget https://dl.grafana.com/oss/release/grafana_6.6.1_armhf.deb -O /tmp/grafana_6.6.1_armhf.deb
dpkg -i /tmp/grafana_6.6.1_armhf.deb
apt-get update
apt-get install -y nginx php7.3-fpm mariadb-server python3-pip python3-pyaudio python3-numpy hostapd dnsmasq iptables-persistent php7.3-mysql

echo "Configuration de l'AP"
systemctl stop hostapd dnsmasq
cat >> /etc/dhcpcd.conf <<FIN
interface wlan0
    static ip_address=192.168.0.10/24
    nohook wpa_supplicant
FIN
mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
cat > /etc/dnsmasq.conf <<FIN
interface=wlan0
  dhcp-range=192.168.0.11,192.168.0.30,255.255.255.0,24h
FIN
cat > /etc/hostapd/hostapd.conf <<FIN
interface=wlan0
driver=nl80211
ssid=noisyroom
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=noisyroom
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
FIN
echo DAEMON_CONF="/etc/hostapd/hostapd.conf" >> /etc/default/hostapd
systemctl unmask hostapd
systemctl enable hostapd
# https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/
echo "Désactivation d'IPv6 et du forward IPv4"
cat > /etc/sysctl.conf <<FIN
net.ipv4.ip_forward = 0
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
net.ipv6.conf.eth0.disable_ipv6 = 1
net.ipv6.conf.wlan0.disable_ipv6 = 1
FIN
sysctl -p


python3 -m pip install pymysql mysql_connector
mysql_secure_installation
mysql -p < database.sql
rm /etc/nginx/sites-enabled/default
mv nginx/noisyroom /etc/nginx/sites-enabled/
systemctl stop grafana
mv /etc/grafana /etc/grafana_BAK
mv grafana /etc/
systemctl enable mariadb grafana-server nginx php7.3-fpm mariadb
systemctl restart mariadb grafana-server nginx php7.3-fpm mariadb
useradd -r noisyroom
echo "noisyroom ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/noisyroom
cat<<FIN
    *** MERCI DE CONFIGURER PHP pour qu'il utilise LE COMPTE 'noisyroom' ***
FIN
