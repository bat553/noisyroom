-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.1.41-MariaDB-0+deb9u1 - Debian 9.9
-- SE du serveur:                debian-linux-gnu
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Creation utilisateur
CREATE USER 'noisyroom'@'localhost' IDENTIFIED BY 'noisyroom';

-- Listage de la structure de la base pour noisyroom
CREATE DATABASE IF NOT EXISTS `noisyroom` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `noisyroom`;

-- Ajout des permissions
GRANT ALL ON noisyroom.* TO 'noisyroom'@'localhost';

-- Listage de la structure de la table noisyroom. data
CREATE TABLE IF NOT EXISTS `data` (
  `id_sample` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_session` int(11) unsigned NOT NULL,
  `dba` int(10) unsigned DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_sample`),
  KEY `FK_data_sessions` (`id_session`),
  CONSTRAINT `FK_data_sessions` FOREIGN KEY (`id_session`) REFERENCES `sessions` (`id_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table noisyroom. sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id_session` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `room` varchar(50) DEFAULT NULL,
  `running` int(10) unsigned DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Sessions';

-- Les données exportées n'étaient pas sélectionnées.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
