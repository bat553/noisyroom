import pymysql.cursors
import math

connection = pymysql.connect(host='localhost',
                             user='noisyroom',
                             password='noisyroom',
                             db='noisyroom',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


def newData(dba: int, id_session: int):
    dba = math.fabs(dba)
    try:
        with connection.cursor() as cursor:
            sql = "INSERT INTO `data` (`dba`, `id_session`) VALUES (%s, %s)"
            cursor.execute(sql, (dba, id_session))
        connection.commit()

    except Exception as e:
        print("Exception! " + str(e))


def newSession(name: str, room: str):
    try:
        with connection.cursor() as cursor:
            sql = "INSERT INTO `sessions` (`name`, `room`, `running`) VALUES (%s, %s, 1)"
            cursor.execute(sql, (name, room))
            session_id = connection.insert_id()
        connection.commit()
        return session_id

    except Exception as e:
        print("Exception! " + str(e))


def endSession(id_session: int):
    try:
        with connection.cursor() as cursor:
            sql = "UPDATE `noisyroom`.`sessions` SET `running`='0' WHERE  `id_session`=%s"
            cursor.execute(sql, id_session)
        connection.commit()
        return True

    except Exception as e:
        print("Exception! " + str(e))
