import signal
import pyaudio
import audioop
import numpy
import math
import sys
import database

CHUNK = 2 ** 11
RATE = 44100

if len(sys.argv) < 2:
    print('Merci de spécifier le nom de la session (main.py <Nom de session> [Salle])')
    exit(0)
else:
    sess_name = sys.argv[1]
    if len(sys.argv) == 3:
        room_name = sys.argv[2]
    else:
        room_name = ""

id_session = database.newSession(sess_name, room_name)
print(id_session)
p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paInt16, channels=1, rate=RATE, input=True,
                frames_per_buffer=CHUNK)

rms_array = []
db_average = []
i = 0


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True
        print("Enregistrement et arret...")
        stream.stop_stream()
        stream.close()
        p.terminate()
        database.endSession(id_session)


if __name__ == '__main__':
    killer = GracefulKiller()
    while not killer.kill_now:
        i += 1
        data = stream.read(CHUNK, exception_on_overflow=False)
        rms = audioop.rms(data, 2)
        rms_array.append(rms)
        if i % 50 == 0:
            rms_avg = round(numpy.average(rms_array))
            i = 0
            if rms_avg > 0:
                db = round(20 * math.log10(rms_avg))
                print("%s dBa" % (db))
                database.newData(db, id_session)
                db_average.append(db)
            rms_array = []
            if db_average.__len__():
                print("Moyenne : %s dBa" % (round(numpy.average(db_average))))
